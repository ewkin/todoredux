import {FETCH_LIST_SUCCESS} from "./actions";

const initialState = {};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_LIST_SUCCESS:
            return action.list;
        default:
            return state;
    }
};

export default reducer;