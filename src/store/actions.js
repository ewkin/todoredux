import axiosToDo from "../axios-ToDo";


export const FETCH_LIST_SUCCESS = 'FETCH_LIST_SUCCESS';

export const fetchListSuccess = list => ({type: FETCH_LIST_SUCCESS, list});

export const fetchList = () => {
    return async dispatch => {
        const response = await axiosToDo.get('https://js9-burger-kim-default-rtdb.firebaseio.com/todo.json');
        dispatch(fetchListSuccess(response.data));
    }

};

export const postItem = item => {
    return async dispatch => {
        try {
            await axiosToDo.post('https://js9-burger-kim-default-rtdb.firebaseio.com/todo.json', item);
        } finally {
            dispatch(fetchList());
        }
    }
};

export const removeItem = id => {
    return async dispatch => {
        try {
            await axiosToDo.delete('https://js9-burger-kim-default-rtdb.firebaseio.com/todo/'+id+'.json');
        } finally {
            dispatch(fetchList());
        }
    }
};

