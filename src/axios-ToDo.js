import axios from 'axios';

const axiosToDo = axios.create({
    baseURL: 'https://js9-burger-kim-default-rtdb.firebaseio.com/'
});

export default axiosToDo;