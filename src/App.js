import React from "react";

import './App.css';
import ToDo from "./containers/ToDo";

const App = () => {
    return (
        <ToDo/>
    );
};

export default App;
