import React, {useEffect} from 'react';
import Task from "../components/Task";
import AddTaskForm from "../components/AddTaskForm";
import {useDispatch, useSelector} from "react-redux";
import {fetchList, postItem, removeItem} from "../store/actions";


const ToDo = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    useEffect(() => {
        dispatch(fetchList());
    }, [dispatch]);

    let taskHolder;
    const addTask = newTask => {
        taskHolder = newTask;
    };

    const removeTask = id => {
        dispatch(removeItem(id));
        console.log(id)

    };

    const postTask = event => {
        event.preventDefault();
        if (taskHolder) {
            dispatch(postItem({task: taskHolder}));
        }

    };


    return (
        <div className='App'>
            <AddTaskForm
                changeTask={addTask}
                handleClick={event => postTask(event)}
            />
            {
                Object.keys(state).map(key => (
                    <Task
                        key={key}
                        text={state[key].task}
                        remove={() => (removeTask(key))}
                    />
                ))
            }
        </div>
    )
};

export default ToDo;