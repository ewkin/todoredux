import React from 'react';
const AddTaskForm = props => {
    return (
        <div className="taskForm">
            <form action="" onSubmit={props.handleClick}>
                <input type="text" placeholder= 'Add new task' onChange={event => props.changeTask(event.target.value)}/>
                <button type="submit">Add</button>
            </form>
        </div>
    );
};

export default AddTaskForm;